//
//  Location.swift
//  RealTimeLocation
//
//  Created by Firdaus Fery A on 07/12/19.
//  Copyright © 2019 Firdaus Fery A. All rights reserved.
//

import Foundation

struct Location {
    var time: String
    var lat: Double
    var long: Double
}
