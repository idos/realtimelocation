//
//  LocationTVC.swift
//  RealTimeLocation
//
//  Created by Firdaus Fery A on 07/12/19.
//  Copyright © 2019 Firdaus Fery A. All rights reserved.
//

import UIKit

class LocationTVC: UITableViewCell {

    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var lat: UILabel!
    @IBOutlet weak var long: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
