//
//  ViewController.swift
//  RealTimeLocation
//
//  Created by Firdaus Fery A on 07/12/19.
//  Copyright © 2019 Firdaus Fery A. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var updateTimer: Timer?
    var backgroundTask: UIBackgroundTaskIdentifier = .invalid
    var second = 0
    var dataLocation: [Location] = [Location]()
    var firstDetectLatLong = false
    private lazy var locationManager: CLLocationManager = {
       let manager = CLLocationManager()
       manager.desiredAccuracy = kCLLocationAccuracyBest
       manager.delegate = self
       manager.requestAlwaysAuthorization()
       manager.allowsBackgroundLocationUpdates = true
       return manager
     }()
    
    private lazy var dateFormatter: DateFormatter = {
      let formatter = DateFormatter()
      formatter.dateStyle = .short
      formatter.timeStyle = .long
      return formatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerTableViewCell()
        NotificationCenter.default.addObserver(self, selector: #selector(reinstateBackgroundTask), name: UIApplication.didBecomeActiveNotification, object: nil)
        // Do any additional setup after loading the view.
        updateTimer = Timer.scheduledTimer(timeInterval: 3.0, target: self,
        selector: #selector(updateMyLocation), userInfo: nil, repeats: true)
        registerBackgroundTask()
        
    }
    
    deinit {
      NotificationCenter.default.removeObserver(self)
    }
    
    func registerTableViewCell() {
        let regis = UINib(nibName: "LocationTVC", bundle: nil)
        self.tableView.register(regis, forCellReuseIdentifier: "LocationTVC")
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func registerBackgroundTask() {
      backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
        self?.endBackgroundTask()
      }
      assert(backgroundTask != .invalid)
    }
    
    func endBackgroundTask() {
      print("Background task ended.")
      UIApplication.shared.endBackgroundTask(backgroundTask)
      backgroundTask = .invalid
    }
    
    @objc func reinstateBackgroundTask() {
      if updateTimer != nil && backgroundTask == .invalid {
        registerBackgroundTask()
      }
    }
    
    
    @objc func updateMyLocation() {
        self.firstDetectLatLong = false
        locationManager.startUpdatingLocation()
        self.second += 3
    }


}

extension ViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let mostRecentLocation = locations.last else {
        return
        }
        self.locationManager.stopUpdatingLocation()
        let now = Date()
        // set data location and time to table
        let data = Location(time: dateFormatter.string(from: now), lat: mostRecentLocation.coordinate.latitude, long: mostRecentLocation.coordinate.longitude)
        if self.firstDetectLatLong == false {
            self.dataLocation.append(data)
            self.firstDetectLatLong = true
        }
        
        self.tableView.reloadData()
        if self.second == 300 {
            self.updateTimer?.invalidate()
            self.endBackgroundTask()
        }
        
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataLocation.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LocationTVC", for: indexPath) as! LocationTVC
        
        cell.time.text = dataLocation[indexPath.row].time
        cell.lat.text = String(dataLocation[indexPath.row].lat)
        cell.long.text = String(dataLocation[indexPath.row].long)
        
        return cell
    }
    
    
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}

